import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

const IconData moon = IconData(0xf71b, fontFamily: 'MaterialIcons');

void main() {
  runApp(WeatherApp());
}

class MyAppTheme {
  static ThemeData appTheme() {
    return ThemeData(
      brightness: Brightness.light,
      iconTheme: IconThemeData(
        color: Colors.white60,
      ),
    );
  }
}

class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: MyAppTheme.appTheme(),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
                flexibleSpace: Container(
                    decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://images.unsplash.com/photo-1558486012-817176f84c6d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=670&q=80"),
                    fit: BoxFit.cover,
                  ),
                )),
                // backgroundColor: Colors.white,
                elevation: 0,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Pattaya",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                )),
          ),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    "https://images.unsplash.com/photo-1558486012-817176f84c6d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=670&q=80"),
                fit: BoxFit.cover,
              ),
            ),
            child: ListView(
              children: <Widget>[
                mainWeatherToday(),
                buildDivider(),
                buildDivider(),
                buildDivider(),
                week()
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            iconSize: 30,
            backgroundColor: Colors.black,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.map_outlined, color: Colors.white),
                label: "",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.location_pin, color: Colors.white),
                label: "",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.menu, color: Colors.white),
                label: "",
              )
            ],
          ),
        ));
  }
}

Widget buildDivider() {
  return const Divider(
    color: Colors.transparent,
  );
}

Widget mainWeatherToday() {
  return Column(
    children: <Widget>[
      Text(
        "26°",
        style: TextStyle(fontSize: 150, height: 1.3,color: Colors.white),
      ),
      Text(
        "Mostly Cloudy",
        style: TextStyle(fontSize: 20,color: Colors.white70),
      ),
      Text(
        "H:31°  L:22°",
        style: TextStyle(fontSize: 20, height: 2,color: Colors.white70),
      )
    ],
  );
}

Widget buildDay() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Today",
            style: TextStyle(fontSize: 22,color: Colors.white),
          ),
          Text(
            "Tuesday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
          Text(
            "Wednesday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
          Text(
            "Thursday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
          Text(
            "Friday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
          Text(
            "Saturday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
          Text(
            "Sunday",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.white),
          ),
        ],
      )
    ],
  );
}

Widget buildIcon() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
          Icon(
            CupertinoIcons.cloud_fill,
            size: 54,
          ),
        ],
      )
    ],
  );
}

Widget week() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white.withOpacity(0.2),
    child: SizedBox(
      height: 450,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            children: <Widget>[
              forecastWeek(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  buildDay(),
                  buildIcon(),
                  buildWeather(),
                ],
              )
            ],
          )
        ],
      ),
    ),
  );
}

Widget forecastWeek() {
  return Row(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(
          top: 20,
          bottom: 20,
          left: 160,
        ),
        child: Text(
          "7-DAY FORECAST",
          style: TextStyle(fontSize: 20,color: Colors.white60 ),
          textAlign: TextAlign.center,

        ),
      ),
    ],
  );
}

Widget buildWeather() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "L:22°  H:31°",
            style: TextStyle(fontSize: 22,color: Colors.black),
          ),
          Text(
            "L:22°  H:30°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
          Text(
            "L:23°  H:31°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
          Text(
            "L:21°  H:31°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
          Text(
            "L:20°  H:30°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
          Text(
            "L:21°  H:28°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
          Text(
            "L:23°  H:32°",
            style: TextStyle(fontSize: 22, height: 2.5,color: Colors.black),
          ),
        ],
      )
    ],
  );
}